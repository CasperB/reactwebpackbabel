## Dev

Before you start make sure you have installed all the dependencies by running `npm install` from the terminal.
Run `npm run dev` to create webpack dev server.

## Build

To bundle and distribute your application run `npm run build`command.