const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');

// constants with paths
const paths = {
  DIST: path.resolve(__dirname, 'dist'),
  SRC: path.resolve(__dirname, 'src'),
  JS: path.resolve(__dirname, 'src/js')
}


// Webpack configuration

module.exports = {
  entry: path.join(paths.JS, 'app.js'),
  output: {
    path: paths.DIST,
    filename: 'app.bundle.js'
  },
  // Use Html plugin
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(paths.SRC, 'index.html'),
    }),
    new ExtractTextPlugin('css/style.bundle.css'),
    new webpack.optimize.UglifyJsPlugin({
      mangle: true,
      compress: {
        warnings: false, // Suppress uglification warnings
        pure_getters: true,
        unsafe: true,
        unsafe_comps: true,
        screw_ie8: true
      },
      output: {
        comments: false,
      },
      exclude: [/\.min\.js$/gi] // skip pre-minified libs
    }),
  ],
  devtool:'eval-source-map',
  // Loaders
  module:{
    rules:[
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use:[
          'babel-loader',
        ],
      },
      {
        test: /\.less$/,
        loader: ExtractTextPlugin.extract({
          use:[
            {
              loader:'css-loader',
              options:{
                sourceMap: true,
                minimize:true,
              }
            },
            {
              loader:'less-loader',
              options: {
                sourceMap: true
              }
            }
          ]
          
        }),
      },
      {
        test: /\.(jpe?g|png|gif|svg)(\?v=\d+\.\d+\.\d+)?$/i,
        use:[
         {
          loader:'file-loader',
          options:{
            outputPath:'assets/'
          }
         }
        ]
      }
    ],
  },
  resolve:{
    extensions:['.js','.jsx','.less'],
  }
}


