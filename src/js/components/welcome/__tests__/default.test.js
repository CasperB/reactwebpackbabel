import React from 'react';
import { shallow } from 'enzyme';
import Welcome from '../index';

describe('<Welcome />', () => {
  
  it('should render Welcome component ', () => {
    const component = shallow(<Welcome />);
    expect(component).toHaveLength(1);
  });

});
