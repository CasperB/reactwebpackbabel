import React, { Component } from 'react'
import './style.less';
import asset from '../../../assets/RWB.png';

class Welcome extends Component {
  render () {
    return (
      <div>
       <img src={asset} />
       <h1>Welcome on the internet.</h1>
       <p className="blue">Simple dev environment for building React application.</p>
      </div>
    );
  };
};

export default Welcome;
